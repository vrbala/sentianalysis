from BeautifulSoup import BeautifulSoup
import re
import urllib

visited = {}
BASE_URL = 'http://www.glassdoor.co.in'

FIRST_PAGE = BASE_URL + '/Reviews/Manhattan-Associates-Reviews-E7866.htm'

PROS_FILE = 'C:\\tries\\glassdoor_ma_pros.txt'
CONS_FILE = 'C:\\tries\\glassdoor_ma_cons.txt'

_RE = re.compile('<a href="(/Reviews/Manhattan-Associates-Company-Reviews-E7866.*\.htm)')

class AppURLOpener(urllib.FancyURLopener):
    version = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0'

urllib._urlopener = AppURLOpener()

def retrieve_url(url):
    '''
    retrieve the url
    get all the review links
    populate PROs and CONs
    '''

    print 'visiting url: ', url
    visited[url] = 1

    f = urllib.urlopen(url)
    c = f.readlines()
    f.close()

    c = ''.join([x for x in c])

    soup = BeautifulSoup(c)

    urls = soup.findAll('a')

    urls = [_RE.search(str(u)) for u in urls]
    urls = [u.group(1) for u in urls if u is not None]

    pros = soup.findAll('p', {'class': 'pro'})
    cons = soup.findAll('p', {'class': 'con'})

    with open(PROS_FILE, 'a') as f:
        for tag in pros:
            f.write(tag.findAll('tt', {'class': 'notranslate'})[0].text + '\n')

    with open(CONS_FILE, 'a') as f:
        for tag in cons:
            f.write(tag.findAll('tt', {'class': 'notranslate'})[0].text + '\n')
    
    return urls

def run():
    urls = [FIRST_PAGE]

    while len(urls):
        try:
            _urls = retrieve_url(urls.pop(0))
        except:
            pass
        for url in _urls:
            url = BASE_URL + url
            if visited.get(url) != 1:
                visited[url] = 1
                urls.append(url)

run()

print 'Done'
