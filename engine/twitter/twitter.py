import oauth2 as oauth
import urllib
import json
import csv

FILENAME = 'C:\\tries\\search_ma_twitter2.csv'

ERROR_FILE = 'C:\\tries\\failed_tweets.txt'

FIELDNAMES = ['text', 'contributors',  'truncated',  'in_reply_to_status_id',  'id',  'favorite_count',  'source',  'retweeted',  'coordinates',  'entities',  'in_reply_to_screen_name',  'in_reply_to_user_id',  'retweet_count',  'id_str',  'favorited',  'user',  'geo',  'in_reply_to_user_id_str',  'possibly_sensitive',  'lang',  'created_at',  'in_reply_to_status_id_str',  'place',  'metadata', 'retweeted_status']

# set these values appropriately
__CONSUMER_KEY__ = ''
__CONSUMER_SECRET__ = ''
__OAUTH_TOKEN__ = ''
__OAUTH_SECRET__ = ''
__ACCESS_TOKEN__ = ''
__ACCESS_SECRET__ = ''
    
# Create your consumer with the proper key/secret.
consumer = oauth.Consumer(key=__CONSUMER_KEY__, 
    secret=__CONSUMER_SECRET__)

# Request token URL for Twitter.
request_token_url = "https://api.twitter.com/oauth/request_token"

access_token_url = 'https://api.twitter.com/oauth/access_token'

'''
# Create our client.
client = oauth.Client(consumer)

# The OAuth Client request works just like httplib2 for the most part.
#resp, content = client.request(request_token_url, "GET")
#print resp
#print content

'''

'''
oauth_token = __OAUTH_TOKEN__
oauth_secret = __OAUTH_SECRET__

oauth_verifier = '8232191'

token = oauth.Token(oauth_token,oauth_secret)
token.set_verifier(oauth_verifier)
client = oauth.Client(consumer, token)

resp, content = client.request(access_token_url, "POST")

print resp
print
print content
'''

oauth_access_token = __ACCESS_TOKEN__
oauth_access_token_secret=__ACCESS_SECRET__
user_id=33495107
screen_name='veeaarbee'

token = oauth.Token(oauth_access_token, oauth_access_token_secret)
client = oauth.Client(consumer, token)

tweet_search_url = 'https://api.twitter.com/1.1/search/tweets.json'
base_query = [('q', '"Logistics"')]

base_query = urllib.urlencode(base_query)
base_query = '?'+base_query

def write_tweets(tweets):

    e = open(ERROR_FILE, 'a')
    with open(FILENAME, 'a') as f:
        csv_writer = csv.DictWriter(f, FIELDNAMES)
        csv_writer.writeheader()
        for tweet in tweets:
            try:
                csv_writer.writerow(tweet)
            except:
                e.write(str(tweet))

    e.close()

def run():

    url = tweet_search_url + base_query
    while True:
        resp, content = client.request(url, "GET")

        print resp
        print

        content = json.loads(unicode(content))
        tweets = content['statuses']
        write_tweets(tweets)
        print
        next_results = content['search_metadata']['next_results']

        if next_results is None: break

        url = tweet_search_url + next_results

def timeline():
    status_timeline_url = 'https://api.twitter.com/1.1/statuses/home_timeline.json?count=200'

    url = status_timeline_url
    for _ in range(10):

        print url

        resp, content = client.request(url, "GET")

        try:
            if resp['status'] != '200':
                print 'failure : ', url
                print resp
                print
                print content
                break
        except Exception as e:
            print str(e)
            print
            print resp
            break

        content = unicode(content)
        content = content.decode('utf-8')
        tweets = json.loads(content)

        print len(tweets)

        tweets.sort(key=lambda x: x['id'])
        write_tweets(tweets)

        max_id = tweets[0]['id']

        print tweets[0]['id']
        print tweets[-1]['id']

        url = status_timeline_url + '&max_id=' + str(max_id)

    
#timeline()
run()
