import nltk
from nltk.corpus import stopwords
from nltk import bigrams
from nltk import trigrams

custom_stopwords = ['manhattan', 'associates']
words_list = []

def preprocess(entries_tagged):
    
    entries = []
    for (entry, sentiment) in entries_tagged:
        words = [i.lower() for i in entry.split()]
        entries.append((words, sentiment))
    return entries

# supports ngrams by splitting the feature and checking if
# there is any stopword in it
def has_stopword(feature, stopwords):
    
    for x in feature.split():
        if x in stopwords:
            return True
    return False

# pulls out all the words from the entries passed in    
def getwords(entries):
    
    allwords = []
    for (words, sentiment) in entries:
        allwords.extend(words)
    return allwords

# returns the words from the list of entries ordered by their frequency    
def getwordfeatures(entries):
    
    wordfreq = nltk.FreqDist(entries)
    words = wordfreq.keys()
    return words

def feature_extractor(doc):
    doc_words = set(doc)
    features = {}

    for i in words_list:
        features['contains(%s)'%i] = (i in doc_words)
    return features

def train(entries_tagged):

    entries = preprocess(entries_tagged)

    global words_list
    words_list = getwordfeatures(getwords(entries))
    words_list = [i for i in words_list if not has_stopword(i, stopwords.words('english'))]
    words_list = [i for i in words_list if not has_stopword(i, custom_stopwords)]

    training_set = nltk.classify.apply_features(feature_extractor, entries)

    c = nltk.NaiveBayesClassifier.train(training_set)

    return c

def classify(c, entry):

    return c.classify(feature_extractor(entry))

if __name__ == "__main__":
    
    p = open('glassdoor_ma_pros.txt', 'r')
    pos_tweets = p.readlines()

    n = open('glassdoor_ma_cons.txt', 'r')
    neu_tweets = n.readlines()

    pos_tags = ['positive'] * len(pos_tweets)
    neu_tags = ['neutral'] * len(neu_tweets)

    tweets_tagged = zip(pos_tweets, pos_tags) + zip(neu_tweets, neu_tags)

    c = train(tweets_tagged)
    print c.show_most_informative_features(10)
