import classifier

p = open('glassdoor_ma_pros.txt', 'r')
pos_tweets = p.readlines()

n = open('glassdoor_ma_cons.txt', 'r')
neu_tweets = n.readlines()

pos_tags = ['positive'] * len(pos_tweets)
neu_tags = ['neutral'] * len(neu_tweets)

tweets_tagged = zip(pos_tweets, pos_tags) + zip(neu_tweets, neu_tags)
c = classifier.train(tweets_tagged)
print c
print c.show_most_informative_features(30)
print c.classify(classifier.feature_extractor('Good Work environment , good amount of travel'))
