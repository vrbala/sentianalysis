import datetime
import sqlite3
import csv

SQLITEDB = 'C:\\Users\\braman\\manhsentiments\\db.sqlite3'
DATAFILE = 'C:\\tries\\search_ma_twitter1.csv'
GLASSDOORFILE = 'C:\\tries\\glassdoor_ma_cons.txt'

conn = sqlite3.connect(SQLITEDB)

cur = conn.cursor()

insert_query = 'insert into sentimentsweb_entry values (?, ?, ?, ?, ?, ?, ?)'

select_query = 'select max(entry_id) from sentimentsweb_entry'

def get_max_id():
    cur.execute(select_query)
    try:
        row = cur.next()
        max_id = row[0]
    except StopIteration:
        max_id = 0

    if max_id is None: max_id = 0
    return max_id

def load_tweets():
    dh = open(DATAFILE, 'r')

    reader = csv.DictReader(dh)

    max_id = get_max_id()
    max_id = int(max_id)    
    for i, line in enumerate(reader):
        text = line['text']
        i += 1+max_id
        if text is not None and not text == 'text':
            cur.execute(insert_query, (i, 'TW', text, False, False, datetime.datetime.now(), None))

    conn.commit()
    dh.close()

def load_glassdoor_reviews():
    max_id = get_max_id()
    max_id = int(max_id)
    gh = open(GLASSDOORFILE, 'r')
    for i, line in enumerate(gh):
        line = line.replace('\r\n', '')
        i += 1+max_id
        cur.execute(insert_query, (i, 'GD', line, True, True, datetime.datetime.now(), -1))

    conn.commit()
    gh.close()

load_tweets()
cur.close()
conn.close()
