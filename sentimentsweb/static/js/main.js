var SE = SE || {};

function xhr() {
    return new XMLHttpRequest;
}

function getRowFluidDiv() {
    var n = document.createElement('div');
    n.className = "row-fluid";
    return n;
}

function getSpan2Div() {
    var n = document.createElement('div');
    n.className = "span2";
    return n;
}

function getSpan8Div() {
    var n = document.createElement('div');
    n.className = "span8";
    return n;
}

function killChildren(node) {
    while(node.firstChild)
        node.removeChild(node.firstChild);
}

function query() {
    var qp = document.getElementById("query-parameters");
    window.event.preventDefault();
    alert($(this).serializeArray());
    return false;
}
