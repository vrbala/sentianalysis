from django.contrib import admin

from sentimentsweb.models import Entry
from sentimentsweb.models import Sentiment

admin.site.register(Entry)
admin.site.register(Sentiment)

