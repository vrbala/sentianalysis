import datetime
from haystack import indexes
from sentimentsweb.models import Entry

# reference: http://django-haystack.readthedocs.org/en/latest/tutorial.html#installation
class EntryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    entry = indexes.CharField(model_attr='text')
    pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return Entry

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
