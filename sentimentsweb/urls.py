from django.conf.urls import patterns, include, url

urlpatterns = patterns('sentimentsweb.views',
    # Examples:
    # url(r'^$', 'manhsentiments.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^/?$', 'index'),
    url(r'/search/(?P<data_source>\w+)', 'search'),
    url(r'/results/(?P<data_source>\w+)', 'results'),    
    url(r'/train', 'train'),
    url(r'/about', 'about'),    
)
