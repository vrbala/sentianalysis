from django.db import models

TWITTER = 'TW'
GLASSDOOR = 'GD'

DATA_SOURCE_CHOICES = (
        (TWITTER, 'Twitter'),
        (GLASSDOOR, 'GlassDoor'),
        )

POSITIVE = 1
NEUTRAL = 0
NEGATIVE = -1

OPINION_CHOICES = (
    (POSITIVE, 'Positive'),
    (NEUTRAL, 'Neutral'),
    (NEGATIVE, 'Negative'),
    )

# contains a tweet or an article
class Entry(models.Model):

    entry_id = models.AutoField(primary_key=True)
    source = models.CharField(max_length=2, choices=DATA_SOURCE_CHOICES)
    text = models.CharField(max_length=10240)
    labeled = models.BooleanField(default=False)
    reviewed = models.BooleanField(default=False)
    pub_date = models.DateTimeField(auto_now=True)
    opinion = models.IntegerField(choices=OPINION_CHOICES, null=True)

class Sentiment(models.Model):

    sentiment_id = models.AutoField(primary_key=True)
    source = models.CharField(max_length=2, choices=DATA_SOURCE_CHOICES)
    positive = models.FloatField()
    negative = models.FloatField()
    neutral = models.FloatField()



    
    
    
