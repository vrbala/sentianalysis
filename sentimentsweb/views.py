import logging
import hashlib
import json
import time

from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django import forms
from django.forms.formsets import formset_factory
from django.forms.widgets import HiddenInput

from haystack.query import SearchQuerySet

from sentimentsweb import models
import classifier

# logger
log = logging.getLogger(__name__)

def get_template(request, template, subs):
    template = loader.get_template(template)
    context = RequestContext(request, subs)
    return template.render(context)

def about(request):
    return HttpResponse(get_template(request, 'about.html', {}))

def index(request):
    return HttpResponse(get_template(request, 'intro.html', {}))
#return HttpResponseRedirect('/manh/stats/all')

def stats(request, data_source=None):
    return HttpResponse(get_template(request, 'stats.html', {}))

class EntryForm(forms.Form):
    source = forms.CharField(label='Data Source:')
    source.widget.attrs['readonly'] = True
    text = forms.CharField(widget=forms.Textarea, label='Data:', max_length=10240)
    text.widget.attrs['readonly'] = True
    opinion = forms.ChoiceField(label='Opinion:', choices=models.OPINION_CHOICES)
    entry_id = forms.IntegerField(widget=HiddenInput())

EntryFormSet = formset_factory(EntryForm, extra=1, max_num=1)
    
def train(request):
    ''' pull any unlabeled entry and ask the user to label it
    '''
    if request.method == 'GET':
        
        entry = models.Entry.objects.filter(opinion=None)[0]

        if entry is not None:
            formset = EntryFormSet(initial=[{
            'source': dict(models.DATA_SOURCE_CHOICES)[entry.source],
            'text': entry.text,
            'entry_id': entry.entry_id
            }])

            # query the labeled samples forthe current data source.
            # train the classifier and label the new entry
            labeled_entries = models.Entry.objects.filter(source=entry.source).filter(labeled=True)
            tagged_entries = []
            for e in labeled_entries:
                tagged_entries.append((e.text, dict(models.OPINION_CHOICES)[e.opinion]))

            # get a classifier
            c = classifier.train(tagged_entries)
            logging.info(str(tagged_entries[:10]))
            o = c.classify(classifier.feature_extractor(entry.text))

        else:
            formset = EntryFormSet()
            
        return HttpResponse(get_template(request, 'train.html',
                                         {'formset': formset,
                                          'opinion': o,}))
    else:
        formset = EntryFormSet(request.POST)

        if formset.is_valid():
            data = formset.cleaned_data[0]
            logging.info(str(data))
            entry = models.Entry.objects.get(pk=data['entry_id'])
            entry.opinion = data['opinion']
            entry.labeled = True
            entry.reviewed = True
            entry.save()
        return HttpResponseRedirect('/manh/train')

def search(request, data_source=None):
    query_string = request.GET.get('q', None)

    # if no search term, render search page again
    if query_string is None or query_string == '':
        return HttpResponse(get_template(request, 'search.html', {}))

    logging.info(' query string: ' + query_string)
    
    all_entries = SearchQuerySet().filter(content=query_string)

    if data_source == 'twitter':
        source = models.TWITTER
    elif data_source == 'glassdoor':
        source = models.GLASSDOOR
    else:
        source = None

    positive = 0
    negative = 0
    neutral = 0
    for e in all_entries:
        entry = models.Entry.objects.get(pk=e.pk)
        # TODO: move this to search above ... else, it pulls lot of data unnecessarily
        if source is not None and source != entry.source:
            continue
        if entry.opinion == 1:
            positive += 1
        elif entry.opinion == -1:
            negative += 1
        else:
            neutral += 1

    logging.info('positive: %d negative: %d neutral: %d' % (positive, negative, neutral))

    return HttpResponse(get_template(request, 'stats.html',
                                     {'chart_title': query_string,
                                      'positive': positive,
                                      'negative': negative,
                                      'neutral': neutral,
                                      'total': positive+negative+neutral,
                                      'query_string': query_string,
                                      'data_source': data_source,
                                      }))
    

def results(request, data_source=None):
    query_string = request.GET.get('q', None)

    logging.info(' query string: ' + query_string)
    
    all_entries = SearchQuerySet().filter(content=query_string)

    if data_source == 'twitter':
        source = models.TWITTER
    elif data_source == 'glassdoor':
        source = models.GLASSDOOR
    else:
        source = None

    results = []
    for e in all_entries:
        entry = models.Entry.objects.get(pk=e.pk)
        # TODO: move this to search above ... else, it pulls lot of data unnecessarily
        if source is not None and source != entry.source:
            continue
        opinion = entry.opinion
        if opinion is None:
            opinion = models.NEUTRAL
            
        results.append({'source': dict(models.DATA_SOURCE_CHOICES)[entry.source],
                        'text': entry.text,
                        'opinion': dict(models.OPINION_CHOICES)[opinion],
                        })
        
    return HttpResponse(get_template(request, 'results.html',
                                     {'results': results,}))
    
